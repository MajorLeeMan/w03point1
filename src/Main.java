/*
Use the following requirements when writing your program:

    Create a program that gathers input of two numbers from the keyboard in its
    main method and then calls the method below to calculate.
    Write a method that divides the two numbers together (num1/num2) and returns
    the result. The method should include a throws clause for the most specific
    exception possible if the user enters zero for num2.
    In your main method, use exception handling to catch the most specific
    exception possible. Display a descriptive message in the catch to tell the
    user what the problem is. Give the user a chance to retry the data entry.
    Display a message in the final statement.
    Run and show the output when there is no exception and when a zero in the
    denominator is used.
    Rewrite the data collection so that it uses data validation to ensure an
    error does not occur. If the user does type a zero in the denominator,
    display a message and give the user a chance to retry.
    Run and show the output when there is no problem with data entry and when a
    zero is used in the denominator.
 */

import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {

    public static void main(String[] args) {
        //starting variables
        Scanner keyboard = new Scanner(System.in);
        int num1=0;
        int num2=0;
        double result=0;
        int i=0;
        String user;

        //tell the user what the program does
        System.out.format("%nThis program takes your first number and " +
                "%ndivides it using the second number." +
                "%nLet's get started!");

        while (i==0) { //loop for asking all the questions
            while(i==0) { //loop for asking and validating the two numbers
                //System.out.format("%nStart of Loop");
                i=1;//if all goes well, exit the loop
                try {
                    //System.out.format("%nStart of Try");
                    //prompt the user for the first number
                    System.out.format("%nPlease enter your first number: ");
                    num1 = keyboard.nextInt();

                } catch (InputMismatchException letter1) {
                    //test.setVar1(num1);
                    System.out.format("%nThat's not an integer.");
                    keyboard.nextLine();
                    i=0;
                }

            }
            i=0; //reset loop counter
            while(i==0) {
                i=1; //if all goes well, exit the loop
                try {
                    //prompt the user for a second number
                    System.out.format("Divide your number by: ");
                    num2 = keyboard.nextInt();

                } catch (InputMismatchException letter2) {
                    //test.setVar2(num2);
                    System.out.format("%nThat's not an integer.");
                    keyboard.nextLine();
                    i=0;
                }

            }

            //once the two numbers are obtained, calculate the division
            //Check ONLY for a specific error (div by 0), tell the user, and give them a retry
            i=0; //reset loop counter
            try {
                result = calculate(num1, num2);
                //display the result
                System.out.format("The result is %.5f", result);
                i=1; //exit the while loop if all completes
            } catch (ArithmeticException zero) {
                i=1;
                while (i==1) {
                    System.out.format("You tried to divide by zero. Try again?" +
                            "%ny/n :");
                    user = keyboard.next();
                    switch (user) {
                        case "n": //exit the program
                            System.out.format("%nK, bye.");
                            System.exit(0);
                            break;

                        case "y": //go back and try again
                            i=0;
                            break;

                        default: //invalid response
                            i=1;
                            System.out.format("%nPlease enter y or n for yes or no respectively.");
                    }
                }
            }
        }
    }

    //methods in use
    /**
     * calculate: divides num1 by num2
     * @param num1 the first double entered by user
     * @param num2 the second double entered by user
     * @return the result of the division
     */
    //method to calculate
    public static double calculate(double num1, double num2) {
        if (num2==0) {throw new ArithmeticException();}
        double n=(num1 / num2);
        return n;
    }
}
